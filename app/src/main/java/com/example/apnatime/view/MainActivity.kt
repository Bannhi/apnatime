package com.example.apnatime.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.service.autofill.UserData
import android.view.Menu
import android.widget.EditText
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.apnatime.R
import com.example.apnatime.model.User
import com.example.apnatime.model.db.ApnaDataBase
import com.example.apnatime.viewmodel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var mainViewModel: MainViewModel
    // var displayUsers: MutableList<User> = ArrayList()
    var users: MutableList<User> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        //loadDataFromDB()
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = LinearLayoutManager(this)


    }

    /**
     * show data from db
     */
    private fun loadDataFromDB() {
        val db = ApnaDataBase.invoke(this)
        mainViewModel.fetchAllUsers(db)?.observe(this@MainActivity, Observer { users ->
            this.users = users as MutableList<User>
        })

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        val searchItem = menu.findItem(R.id.menu_search)
        if (searchItem != null) {
            val searchView = searchItem.actionView as SearchView
            val editext = searchView.findViewById<EditText>(R.id.search_src_text)
            editext.hint = "Search here..."

            searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
                override fun onQueryTextSubmit(query: String?): Boolean {
                    return true
                }

                override fun onQueryTextChange(newText: String?): Boolean {

                    //displayUsers.clear()
                    if (newText!!.isNotEmpty()) {
                        val search = newText.toLowerCase()
                        /*  users.forEach {
                              if (it.name.toLowerCase().contains(search)) {
                                  displayUsers.add(it)
                              }
                          }*/
                        //if (displayUsers.isEmpty()) {
                        mainViewModel.getUser(search)
                            .observe(this@MainActivity, Observer<User?> { user ->
                                users.clear()
                                if(user!=null) {
                                    users.add(users.size, user)
                                }
                                recyclerView?.adapter =
                                    UserAdapter(users as ArrayList<User>)
                            })
                        // }
                    }
                    recyclerView.adapter?.notifyDataSetChanged()
                    return true
                }

            })
        }

        return super.onCreateOptionsMenu(menu)
    }
}
