package com.example.apnatime.view

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.apnatime.R
import com.example.apnatime.model.Repo

class RepoAdapter(private val repo: ArrayList<Repo>) :
    RecyclerView.Adapter<RepoAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.repo, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        // holder.textViewName.text = users[position].name
    }

    override fun getItemCount(): Int {
        return repo.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        // var textViewName: TextView = itemView.findViewById(R.id.textViewName)

    }
}