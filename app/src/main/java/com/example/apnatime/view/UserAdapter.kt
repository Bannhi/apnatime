package com.example.apnatime.view

import android.content.Context
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.example.apnatime.R
import com.example.apnatime.model.User


class UserAdapter(private val users: ArrayList<User>) :
    RecyclerView.Adapter<UserAdapter.ViewHolder>() {

    lateinit var context: Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_layout, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvUSerName.text = users[position].name
        holder.tvName.text = users[position].name
        Glide.with(context)
            .load(users[position].imageUrl)
            .into(holder.photo)

    }

    override fun getItemCount(): Int {
        return users.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var tvUSerName: TextView = itemView.findViewById(R.id.user_name)
        var tvName: TextView = itemView.findViewById(R.id.name)
        var photo: ImageView = itemView.findViewById(R.id.photo)

    }
}