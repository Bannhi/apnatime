package com.example.apnatime.model

import com.google.gson.annotations.SerializedName

class Repo {


    @SerializedName("description")
    var description: String = ""
    @SerializedName("url")
    var url: String = ""
    @SerializedName("name")
    var name: String = ""
    @SerializedName("watchers_count")
    var starsCount: String = ""



}