package com.example.apnatime.model.db.user

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.apnatime.model.db.repo.RepoData

@Dao
interface RepoDataDao {

    /**
     * function to fetch all users which contains this string
     */
    @Query("SELECT * from repo_table WHERE userName LIKE :name")
    fun getAllStoredUsers(name: String): List<RepoData>

    /**
     * insert the fetched user
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertRepo(repo: RepoData)


}