package com.example.apnatime.model.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.apnatime.model.db.repo.RepoData
import com.example.apnatime.model.db.user.RepoDataDao
import com.example.apnatime.model.db.user.UserData
import com.example.apnatime.model.db.user.UserDataDao

@Database(
    entities = [RepoData::class, UserData::class],
    version = 1
)
abstract class ApnaDataBase : RoomDatabase() {

    abstract fun repoDataDao(): RepoDataDao
    abstract fun userDataDao(): UserDataDao


    companion object {
        @Volatile private var instance: ApnaDataBase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context,
            ApnaDataBase::class.java, "apna_time.db")
            .build()
    }
}