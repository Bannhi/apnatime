package com.example.apnatime.model.db.user

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface UserDataDao {

    /**
     * function to fetch all users which contains this string
     */
    @Query("SELECT * from user_table WHERE name LIKE :name")
    fun getAllStoredUsers(name: String): List<UserData>

    /**
     * function to fetch all users which contains this string
     */
    @Query("SELECT * from user_table")
    fun getAllUsers(): LiveData<List<UserData>>

    /**
     * insert the fetched user
     */
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertUser(user: UserData)


}