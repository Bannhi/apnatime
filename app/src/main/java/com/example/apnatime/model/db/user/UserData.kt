package com.example.apnatime.model.db.user

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "user_table")
data class UserData(

    @PrimaryKey
    @ColumnInfo(name = "userName") var userName: String,

    @ColumnInfo(name = "photo") var photo: String,
    @ColumnInfo(name = "name") var name: String,
    @ColumnInfo(name = "repo") var repo: String

)

