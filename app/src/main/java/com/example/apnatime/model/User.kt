package com.example.apnatime.model

import com.google.gson.annotations.SerializedName

class User {

    @SerializedName("repos_url")
    var reposUrl: String = ""
    var id: String = ""
    var email: String = ""
    @SerializedName("avatar_url")
    var imageUrl: String = ""
    var name: String = ""


}