package com.example.apnatime.model.db.repo

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "repo_table")
data class RepoData(

    @PrimaryKey
    @ColumnInfo(name = "userName") var userName: String,

    @ColumnInfo(name = "description") var description: String,
    @ColumnInfo(name = "link") var link: String,
    @ColumnInfo(name = "stars_count") var stars: Int


)

