package com.example.apnatime.network
import com.example.apnatime.model.Repo
import com.example.apnatime.model.User
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url




interface ApiClient {

    @GET("users/{username}")
    fun getUser(
        @Path("username") name: String
    ): Call<User>

    @GET
    fun getRepos(@Url url: String
    ): Call<List<Repo>>

    companion object {

        val BASE_URL = "https://api.github.com/"
    }
}