package com.example.apnatime.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.apnatime.model.Repo
import com.example.apnatime.model.User
import com.example.apnatime.model.db.ApnaDataBase
import com.example.apnatime.model.db.user.UserData
import com.example.apnatime.network.ApiClient
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainViewModel : ViewModel() {

    //this is the data that we will fetch asynchronously
    private var user: MutableLiveData<User>? = null

    //this is the data that we will fetch asynchronously
    private var repoList: MutableLiveData<List<Repo>>? = null

    //this is the data that we will fetch asynchronously
    private var users: LiveData<List<UserData>>? = null

    //we will call this method to get the data
    fun getUser(name: String): LiveData<User> {
        user = MutableLiveData()
        fetchUserDetails(name)

        //finally we will return the user
        return user as MutableLiveData<User>
    }

    /**
     * get the repos
     */
    fun getRepos(url: String): LiveData<List<Repo>> {
        //if the list is null
        if (repoList == null) {
            repoList = MutableLiveData()
            //we will load it asynchronously from server in this method
            fetchRepos(url)
        }

        //finally we will return the list
        return repoList as MutableLiveData<List<Repo>>
    }

    /**
     * function to fetch the user details by username
     */
    private fun fetchUserDetails(name: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(ApiClient.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(ApiClient::class.java)
        val call = api.getUser(name)

        call.enqueue(object : Callback<User> {
            override fun onFailure(call: Call<User>, t: Throwable) {
                user!!.value = null
            }

            override fun onResponse(call: Call<User>, response: Response<User>) {
                println("RESPONSE: " + response.body()?.email)
                user!!.value = response.body()
            }
        })
    }

    /**
     * function to get the repos of the user.
     */
    private fun fetchRepos(url: String) {
        val retrofit = Retrofit.Builder()
            .baseUrl(ApiClient.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        val api = retrofit.create(ApiClient::class.java)
        val call = api.getRepos(url)

        call.enqueue(object : Callback<List<Repo>> {
            override fun onFailure(call: Call<List<Repo>>, t: Throwable) {
            }

            override fun onResponse(call: Call<List<Repo>>, response: Response<List<Repo>>) {
                println("RESPONSE: " + response.body()?.get(0)?.name)

                //TODO add the user object.
            }
        })
    }

    fun fetchAllUsers(db: ApnaDataBase): LiveData<List<UserData>>? {

        GlobalScope.launch {
            users = db.userDataDao().getAllUsers()

        }
        if (users != null) {
            return users as MutableLiveData<List<UserData>>
        } else {
            return users
        }
    }

}